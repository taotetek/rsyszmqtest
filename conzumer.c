#include <czmq.h>

static int
s_handle_event (zloop_t *loop, zmq_pollitem_t *poller, void *arg)
{
    void *listener = arg;
    zmsg_t *msg = zmsg_recv (listener);

    if (msg) {
        zframe_t *t_frame = zmsg_unwrap (msg);
        char *line = zframe_strdup (t_frame);
        zframe_destroy (&t_frame);
        printf ("%s\n", line);
        zmsg_destroy (&msg);
    }
    return 0;
}

int
main (void)
{
    zctx_t *ctx = zctx_new ();
    
    void *listener = zsocket_new (ctx, ZMQ_PULL);
    zsocket_bind (listener, "tcp://127.0.0.1:5556");
     
    zloop_t *loop = zloop_new ();
    zloop_set_verbose (loop, 1);
    zmq_pollitem_t listener_p = { listener, 0, ZMQ_POLLIN };
    int rc = zloop_poller (loop, &listener_p, s_handle_event, listener);
    if (rc !=0 )
        goto failed;

    zloop_start (loop);
    zloop_destroy (&loop);
    zctx_destroy (&ctx);
    
    return EXIT_SUCCESS;
failed:
    return EXIT_FAILURE;
}
